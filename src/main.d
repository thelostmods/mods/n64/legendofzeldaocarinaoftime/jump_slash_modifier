module main;

import crossemu.sdk.n64;
import crossemu.gdk.n64.loz_oot;

import std.stdio : writeln;

mixin(initPlugin!(
    "Jump Slash Modifier",
    "Changes Link's Jumpslashes to one of the three unused ones. " ~
    "Frontflip, Backflip and Slicer.",
    0x01_00_00_00, // 1.0.0.0,
    false
));

Animations curAnim = Animations.FrontFlip;
enum Animations
{
    Default,
    BackFlip,
    FrontFlip,
    Slicer
}

bool initialize()
{
    // make sure gdk loads
    if (!gdkInitialize())
        return false;
    
    // make sure we are using the expected version
    if (currentVersion != GameVersion.NTSC_1_0)
        return false;
    
    // everything checked out
    return true;
}

void terminate()
{
    gdkTerminate();
}

void onFirstFrame()
{
	gdkOnFirstFrame();
}

void onTick(uint frame)
{
    // process any gdk tick stuff first
    gdkOnTick(frame);

    // don't do weird stuff on title/load screens
    if (!player.exists) return;

    // inject our prefered animation
    switch (curAnim)
    {
        default:
		case Animations.Default: rdram.write!ushort(0x803AAB02, 0x2900); break;
		case Animations.BackFlip: rdram.write!ushort(0x803AAB02, 0x29D0); break;
		case Animations.FrontFlip: rdram.write!ushort(0x803AAB02, 0x2A60); break;
		case Animations.Slicer: rdram.write!ushort(0x803AAB02, 0x2AD0); break;
    }
}